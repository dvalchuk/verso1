#include "global.h"

#include <cstdlib>
#include <iostream>


int main(int argc, char* argv[]) {
    try {
        bpo::options_description generic("Generic options");
        generic.add_options()
            ("version,v", "print version string")
            ("help", "produce help message")
            ;

        bpo::options_description config("Configuration [~/.vservrc]");
        std::string host_opt;
        unsigned int port_opt;
        config.add_options()
            ("host,h", bpo::value<std::string>(&host_opt)->default_value("127.0.0.1"),
             "listen address")
            ("port,p",
             bpo::value<unsigned int>(&port_opt)->default_value(80),
             "listen port")
            ;

        bpo::options_description cmdline_options;
        cmdline_options.add(generic).add(config);

        bpo::options_description config_file_options;
        config_file_options.add(config);

        bpo::options_description visible("Allowed options");
        visible.add(generic).add(config);

        bpo::variables_map vm;
        const bfs::path rc_filepath("~/.vservrc");
        bool is_rc_exists = bfs::exists(rc_filepath) && bfs::is_regular_file(rc_filepath);
        if (is_rc_exists) {
            std::fstream f(rc_filepath.string().data(), std::ios::in | std::ios::binary);
            bpo::store(bpo::parse_config_file(f, config_file_options), vm);
        }
        bpo::store(bpo::parse_command_line(argc, argv, cmdline_options), vm);
        bpo::notify(vm);

        if (vm.count("help")) {
            std::cout << visible << std::endl;
            return EXIT_SUCCESS;
        }
    } catch (const std::exception& ex) {
        std::cerr << "Unhandled exception: " << ex.what() << std::endl;
    }

    return EXIT_SUCCESS;
}
