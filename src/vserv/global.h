#ifndef __VS_GLOBAL_H__
#define __VS_GLOBAL_H__

#pragma once

#include <string>
#include <fstream>

#define BOOST_ALL_NO_LIB
#include <boost/noncopyable.hpp>
#include <boost/program_options.hpp>
namespace bpo = boost::program_options;
#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>
namespace bfs = boost::filesystem;

#if defined(_MSC_VER)
# define V_FUNC_INFO __FUNCSIG__
#else
# define V_FUNC_INFO __PRETTY_FUNCTION__
#endif

#define V_UNUSED(x) (void)x;

#endif // __VS_GLOBAL_H__
