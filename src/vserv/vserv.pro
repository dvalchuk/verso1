TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXX = ccache g++

LIBS +=  -lboost_program_options -lboost_filesystem -lboost_system

HEADERS += \
    global.h \

SOURCES += \
    main.cpp \
