#include "local_data_source.h"
#include "local_index.h"
#include "remote_index.h"
#include "local_changes_index.h"
#include "download_processor.h"
#include <vcore/index.h>
#include <vcore/utils.h>

namespace verso {

local_data_source::local_data_source(const std::string& url, const boost::filesystem::path& root)
    : data_source(url, root)
    , _remote_root(url) {
}

std::string local_data_source::get_index_sha() {
    const bfs::path remote_index_dir(_remote_root / index::index_dir_name());
    if (bfs::exists(remote_index_dir)) {
        const bfs::path remote_index_file(remote_index_dir / local_index::index_file_name());
        if (bfs::exists(remote_index_file)) {
            const bfs::path remote_index_sha = index::sha_path(remote_index_file);
            local_changes_index remote_local_changes(_remote_root);
            if (remote_index_sha != remote_local_changes.sha()) {
                std::stringstream ss;
                ss << "Remote repository [" << _remote_root << "] contain local changes.";
                ss << "Commit them or discard before synchronization.";
                throw std::logic_error(ss.str());
            }
            if (bfs::exists(remote_index_sha)) {
                return read_sha256(remote_index_sha);
            }
        }
    }
    return std::string();
}

verso::index local_data_source::get_index(const boost::filesystem::path& out) {
    const bfs::path remote_index_dir(_remote_root / index::index_dir_name());
    const bfs::path remote_index_file(remote_index_dir / local_index::index_file_name());
    if (!bfs::exists(remote_index_dir) || !bfs::exists(remote_index_file)) {
        throw std::logic_error(std::string("remote index is not at his place")
                               + remote_index_file.generic_string());
    }
    verso::copy_file(remote_index_file, out);
    remote_index ridx(out);
    return ridx;
}

void local_data_source::get_file(const std::string& path) {
    throw std::logic_error("not implemented");
}

void local_data_source::get_diff(const std::string& sha, const boost::filesystem::path& out) {
    throw std::logic_error("not implemented");
}

void local_data_source::post_diff(const boost::filesystem::path& path) {
    const bfs::path download_dir = _remote_root / download_processor::download_dir_name();
    if (!bfs::exists(download_dir)) {
        bfs::create_directory(download_dir);
    }
    const bfs::path out_diff_path = download_dir / path.filename();
    verso::copy_file(path, out_diff_path);
    download_processor proc(out_diff_path, _remote_root);
    proc.run(); // apply
}

}





