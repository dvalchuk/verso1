#include "dir_walker.h"
#include <vcore/utils.h>

namespace verso {

dir_walker::dir_walker(const boost::filesystem::path& root)
    : _root(root) {
}

void dir_walker::run(const boost::regex& skip_filter, file_action_t callback) {
    scoped_set_current_path current_path_setter(_root);
    for (bfs::recursive_directory_iterator it("./"), et; it != et; ++it) {
        boost::smatch what;
        // Skip whole dir if match
        if(boost::regex_match( it->path().filename().generic_string(), what, skip_filter ))
            it.no_push();

        if(!bfs::is_regular_file( *it ))
            continue;

        if (callback)
            callback(it->path());
    }
}

}
