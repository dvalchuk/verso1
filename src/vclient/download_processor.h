#ifndef DOWNLOAD_PROCESSOR_H
#define DOWNLOAD_PROCESSOR_H

#include "vprocessor.h"

namespace verso {

class diff;

class download_processor : public vprocessor {
    bfs::path _downloaded_diff;
public:
    download_processor(const std::string& remote, const bfs::path& root);
    download_processor(const bfs::path& local_diff, const boost::filesystem::path& root);

    virtual void run();

    static const char* download_dir_name();
};

}

#endif // DOWNLOAD_PROCESSOR_H
