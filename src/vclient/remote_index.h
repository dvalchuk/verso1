#ifndef REMOTE_INDEX_H
#define REMOTE_INDEX_H

#include <vcore/index.h>

namespace verso {

class remote_index : public index
{
public:
    remote_index(const boost::filesystem::path &root);
    ~remote_index();
};

}

#endif // REMOTE_INDEX_H
