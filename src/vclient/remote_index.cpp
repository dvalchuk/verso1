#include "remote_index.h"

namespace verso {

namespace {
const char* const k_remote_index_file_name = "remote.idx";
}

remote_index::remote_index(const bfs::path& root)
    : index(root / index::index_dir_name() / k_remote_index_file_name) {
    if (bfs::exists(path())) {
        open();
    }
}

remote_index::~remote_index() {

}

}
