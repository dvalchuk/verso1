#include "local_changes_index.h"

namespace verso {

namespace {
const char* const k_changes_index_file_name = "changes.idx";
}


local_changes_index::local_changes_index(const bfs::path& root)
    : local_index(root, root / index::index_dir_name() / k_changes_index_file_name)
{
    if (bfs::exists(path())) {
        bfs::remove(path());
    }
    create_new_index();
}

}
