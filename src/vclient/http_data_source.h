#ifndef HTTP_DATA_SOURCE_H
#define HTTP_DATA_SOURCE_H

#include "data_source.h"

namespace verso {

class http_data_source : public data_source
{
public:
    explicit http_data_source(const std::string& url, const bfs::path& root);

    // data_source interface
public:
    virtual std::string get_index_sha();
    virtual index get_index(const boost::filesystem::path& out);
    virtual void get_file(const std::string& path);
    virtual void get_diff(const std::string& sha, const bfs::path& out);
    virtual void post_diff(const boost::filesystem::path& path);
};

}

#endif // HTTP_DATA_SOURCE_H
