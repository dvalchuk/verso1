#ifndef LOCAL_DATA_SOURCE_H
#define LOCAL_DATA_SOURCE_H

#include "data_source.h"

namespace verso {

class local_data_source : public data_source
{
    bfs::path _remote_root;
public:
    local_data_source(const std::string& url, const bfs::path& root);

    // data_source interface
public:
    virtual std::string get_index_sha();
    virtual index get_index(const boost::filesystem::path& out);
    virtual void get_file(const std::string& path);
    virtual void get_diff(const std::string& sha, const bfs::path& out);
    virtual void post_diff(const boost::filesystem::path& path);
};

}

#endif // LOCAL_DATA_SOURCE_H
