#include "http_data_source.h"
#include <vcore/index.h>

namespace verso {

http_data_source::http_data_source(const std::string& url, const boost::filesystem::path& root)
    : data_source(url, root)
{
}

}


std::string verso::http_data_source::get_index_sha()
{
    return std::string();
}

verso::index verso::http_data_source::get_index(const boost::filesystem::path& out)
{
    return index(bfs::path());
}

void verso::http_data_source::get_file(const std::string& path)
{
}

void verso::http_data_source::get_diff(const std::string& sha, const boost::filesystem::path& out) {
}

void verso::http_data_source::post_diff(const boost::filesystem::path& path)
{
}
