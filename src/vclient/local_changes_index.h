#ifndef LOCAL_CHANGES_INDEX_H
#define LOCAL_CHANGES_INDEX_H

#include "local_index.h"

namespace verso {

class local_changes_index : public local_index
{
public:
    explicit local_changes_index(const bfs::path& root);
};

}

#endif // LOCAL_CHANGES_INDEX_H
