TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

TARGET = verso

DEPENDPATH += \
    ../vserv \
    ../vcore

PRE_TARGETDEPS += ../vcore

QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXX = ccache g++

INCLUDEPATH += ./..
LIBPATH += ../vcore

LIBS += -lvcore
LIBS += -lboost_regex -lboost_program_options -lboost_filesystem -lboost_system
LIBS += -lcrypto
LIBS += -lprotobuf

HEADERS += \
    global.h \
    vprocessor.h \
    upload_processor.h \
    dir_walker.h \
    data_source.h \
    local_data_source.h \
    local_index.h \
    http_data_source.h \
    local_changes_index.h \
    remote_index.h \
    download_processor.h

SOURCES += \
    main.cpp \
    vprocessor.cpp \
    upload_processor.cpp \
    dir_walker.cpp \
    local_data_source.cpp \
    local_index.cpp \
    http_data_source.cpp \
    local_changes_index.cpp \
    remote_index.cpp \
    download_processor.cpp
