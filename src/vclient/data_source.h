#ifndef DATA_SOURCE_H
#define DATA_SOURCE_H

#include "global.h"

namespace verso {

class index;

class data_source
{
    std::string _base_url;
    bfs::path _root;
public:
    explicit data_source(const std::string& url, const bfs::path& root)
        : _base_url(url), _root(root) {}
    virtual ~data_source() {}

    virtual std::string get_index_sha() = 0;
    virtual index get_index(const bfs::path& out) = 0;
    virtual void get_file(const std::string& path) = 0;
    virtual void get_diff(const std::string& sha, const bfs::path& out) = 0;
    virtual void post_diff(const bfs::path& path) = 0;
};

}

#endif // DATA_SOURCE_H
