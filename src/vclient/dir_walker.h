#ifndef DIR_WALKER_H
#define DIR_WALKER_H

#include "global.h"

namespace verso {

class dir_walker {
public:
    typedef std::function< void(const bfs::path& fpath) > file_action_t;
private:
    file_action_t _callback;
    bfs::path _root;
public:
    explicit dir_walker(const bfs::path& root);

    void run(const boost::regex& filter, file_action_t callback);
};

}

#endif // DIR_WALKER_H
