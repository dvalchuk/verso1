#ifndef LOCAL_INDEX_H
#define LOCAL_INDEX_H

#include "vcore/index.h"

namespace verso {

class local_index  : public index
{
    bfs::path _root;
    bool _saved;
public:
    explicit local_index(const bfs::path& root);
    virtual ~local_index();

    static const char* index_file_name();

    const bfs::path& root() const { return _root; }

protected:
    explicit local_index(const bfs::path& root, const bfs::path& fpath);
    void create_new_index();
    virtual void save();
    virtual void apply(const diff::operation& op, const boost::filesystem::path &in_root);
};

}

#endif // LOCAL_INDEX_H
