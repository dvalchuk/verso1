#ifndef UPLOAD_PROCESSOR_H
#define UPLOAD_PROCESSOR_H

#include "vprocessor.h"

namespace verso {

class upload_processor : public vprocessor {
public:
    explicit upload_processor(const std::string& remote, const bfs::path& root);

    virtual void run();

private:

};

}

#endif // UPLOAD_PROCESSOR_H
