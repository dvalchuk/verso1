#include "local_index.h"
#include <vcore/utils.h>
#include "dir_walker.h"

#include <vcore/index.pb.h>
#include <boost/filesystem/fstream.hpp>

namespace verso {

namespace {
const char* const k_index_file_name = "main.idx";
}

local_index::local_index(const boost::filesystem::path& root)
    : index(root / index::index_dir_name() / k_index_file_name)
    , _root(root)
    , _saved(false)
{
    if (bfs::exists(path())) {
        open();
    } else {
        create_new_index();
    }
}

local_index::~local_index() {
    save();
}

const char* local_index::index_file_name() {
    return k_index_file_name;  
}

local_index::local_index(const boost::filesystem::path& root, const boost::filesystem::path& fpath)
    : index(fpath)
    , _root(root)
    , _saved(false) {
}

void local_index::create_new_index() {
    bfs::create_directory(_root / index::index_dir_name());

    dir_walker walker(_root);
    const boost::regex self_filter( std::string(index::index_dir_name()) + "/*" );
    dir_walker::file_action_t callback = [&] (const bfs::path& fpath) {
        const std::string pathstr = fpath.generic_string();
        const std::string sha = sha256(fpath);
        insert(sha, pathstr);
    };
    walker.run(self_filter, callback);
    save();
}

void local_index::save()
{
    if (_saved)
        return;

    verso::pb::index pbindex;
    index::kv_action_t callback = [&] (const std::string& sha, const std::string& path) {
        verso::pb::index_item* ii = pbindex.add_index_items();
        ii->set_sha(sha);
        ii->set_path(path);
    };
    walk_items(callback);

    bfs::ofstream pbofs(path(), bfs::ofstream::trunc | bfs::ofstream::binary);
    if (!pbindex.SerializeToOstream(&pbofs)) {
        std::stringstream ss;
        ss << "Failed to serialize index to: " << path();
        throw std::logic_error(ss.str());
    }

    set_sha(sha256(path()));
    bfs::ofstream sha_ofs(sha_path(), bfs::ofstream::binary | bfs::ofstream::trunc);
    if (!sha_ofs) {
        std::stringstream ss;
        ss << "could not open file " << sha_path();
        throw std::logic_error(ss.str());
    }
    sha_ofs.write(sha().data(), sha().size());

    _saved = true;
}

void local_index::apply(const diff::operation &op, const boost::filesystem::path &in_root) {
    switch (op._type) {
    case diff::operation::type::CHANGE:
    case diff::operation::type::ADD: {
        const bfs::path in_file = in_root / op._path;
        const bfs::path out_file = _root / op._path;
        verso::copy_file(in_file, out_file);
    }
        break;
    case diff::operation::type::REMOVE: {
        const bfs::path out_file = _root / op._path;
        bfs::remove(out_file);
    }
        break;
    default: {
        throw std::logic_error("Unknown operation.");
    }
        break;
    }
    index::apply(op, in_root);
}

}
