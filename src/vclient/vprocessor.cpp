#include "vprocessor.h"
#include "local_index.h"
#include "local_changes_index.h"
#include "data_source.h"
#include "local_data_source.h"
#include "http_data_source.h"

#include <vcore/index.h>
#include <vcore/diff.h>

namespace verso {

namespace {
const char* const k_diff = ".verso";
}

vprocessor::vprocessor(const std::string& remote, const boost::filesystem::path& root)
    : _remote_url(remote)
    , _root(root) {
    // it is better to use Factory
    if (_remote_url.find("http://") != std::string::npos) {
        _data_source.reset(new http_data_source(remote, root));
    } else {
        _data_source.reset(new local_data_source(remote, root));
    }
}

vprocessor::~vprocessor() {

}

void vprocessor::run() {
    _local_index.reset(new local_index(_root));
    _local_changes_index.reset(new local_changes_index(_root));
}

data_source& vprocessor::get_data_source() {
    return *_data_source;
}

local_index&vprocessor::get_local_index() {
    return *_local_index;
}

local_changes_index&vprocessor::get_local_changes_index() {
    return *_local_changes_index;
}

}

