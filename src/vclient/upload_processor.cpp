#include "upload_processor.h"
#include "data_source.h"
#include "local_index.h"
#include "local_changes_index.h"
#include <vcore/utils.h>

#include <vcore/diff.h>

namespace verso {

namespace {
const char* const k_packed_diff_dir = "diff_2send";
}

upload_processor::upload_processor(const std::string& remote, const boost::filesystem::path &root)
    : vprocessor(remote, root) {
}

void upload_processor::run()
{
    vprocessor::run();
    data_source& ds = get_data_source();
    local_index& idx = get_local_index();
    local_changes_index& cidx = get_local_changes_index();
    const std::string remote_index_sha = ds.get_index_sha();

    if (remote_index_sha != idx.sha()) {
        throw std::logic_error("Remote index is newer then local one. You should update local copy first.");
    } else {
        if (remote_index_sha != cidx.sha()) {
           diff d = cidx - idx;
           const bfs::path dpack_dir(root() / index::index_dir_name() / k_packed_diff_dir);
           if (bfs::exists(dpack_dir))
               bfs::remove_all(dpack_dir);
           bfs::create_directory(dpack_dir);
           d.set_root(root());
           const bfs::path packed_diff = d.pack(dpack_dir);
           ds.post_diff(packed_diff);
           bfs::remove_all(dpack_dir);
           idx.update_with_index(cidx); // do not touch files
        }
    }
}

}
