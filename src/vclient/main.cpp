#include "global.h"
#include "vprocessor.h"
#include "upload_processor.h"


#include <cstdlib>
#include <iostream>
#include <google/protobuf/service.h>

namespace {
const char* const k_version_str = "v0.1.0";

void conflicting_options(const boost::program_options::variables_map& vm,
                         const std::vector<std::string>& opts) {
    std::size_t sz = 0;
    std::string options_desc;
    for(const std::string& opt: opts) {
        sz += vm.count(opt);
        options_desc += opt;
        options_desc += " ";
    }
    if (sz > 1) {
        throw std::logic_error(std::string("Conflicting options: [") + options_desc + "].");
    }
}

void check_valid_dir(const bfs::path& path) {
    if (!bfs::exists(path))
        throw std::logic_error(std::string("Specified path (") + path.string() + ") does not exists.");
    if (!bfs::is_directory(path))
        throw std::logic_error(std::string("Specified path (") + path.string() + ") is not a dir.");
}

}

int main(int argc, char* argv[]) {
    try {
        GOOGLE_PROTOBUF_VERIFY_VERSION;
        bpo::options_description generic("Generic options");
        generic.add_options()
            ("version,v", "print version string")
            ("help", "produce help message")
            ;

        bpo::options_description config("Configuration");
        std::string remote_opt;
        std::string path_opt;
        config.add_options()
            ("remote,r", bpo::value<std::string>(&remote_opt)->default_value("http://127.0.0.1:80"),
             "connect to address")
            ("upload,u", bpo::value<std::string>(&path_opt),
                 "upload files from specified dir")
            ("download,d", bpo::value<std::string>(&path_opt),
                 "download files to specified dir")
            ("list,l", bpo::value<std::string>(&path_opt),
                 "list files in specified dir")
            ;

        bpo::options_description cmdline_options;
        cmdline_options.add(generic).add(config);

        bpo::options_description visible("Allowed options");
        visible.add(generic).add(config);

        bpo::variables_map vm;
        bpo::store(bpo::parse_command_line(argc, argv, cmdline_options), vm);
        bpo::notify(vm);

        conflicting_options (vm, {"upload", "download", "list"} );

        if (vm.count("help")) {
            std::cout << visible << std::endl;
            return EXIT_SUCCESS;
        }
        if (vm.count("version")) {
            std::cout << "verso client " << k_version_str << std::endl;
            return EXIT_SUCCESS;
        }

        std::unique_ptr <verso::vprocessor> proc;

        // it is better to use Abstract Factory here
        if (vm.count("upload")) {
            const bfs::path root(path_opt);
            check_valid_dir(root);
            proc.reset(new verso::upload_processor(remote_opt, root));
        }

        if (proc)
            proc->run();

        google::protobuf::ShutdownProtobufLibrary();
    } catch (const std::exception& ex) {
        std::cerr << "Unhandled exception: " << ex.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
