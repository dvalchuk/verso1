#include "download_processor.h"
#include <vcore/diff.h>
#include <vcore/index.h>
#include "data_source.h"
#include "local_changes_index.h"
#include "local_index.h"

namespace verso {

namespace {
const char* const k_download_dir_name = "downloaded";
}

download_processor::download_processor(const std::string& remote, const bfs::path& root)
    : vprocessor(remote, root) {
}

download_processor::download_processor(const boost::filesystem::path& local_diff, const boost::filesystem::path& root)
    : vprocessor("", root)
    , _downloaded_diff(local_diff) {

}

void download_processor::run() {
    vprocessor::run();
    const bfs::path download_dir = root() / k_download_dir_name;
    local_index& idx = get_local_index();
    local_changes_index& cidx = get_local_changes_index();
    if (_downloaded_diff.empty()) {
        if (!bfs::exists(download_dir)) {
            bfs::create_directory(download_dir);
        }
        data_source& ds = get_data_source();
        _downloaded_diff = download_dir / diff::diff_pack_file_name();
        ds.get_diff(idx.sha(), _downloaded_diff);
    }
    diff downloaded(_downloaded_diff);
    if (idx.sha() != cidx.sha()) {
        diff local_diff = cidx - idx;
        local_diff.set_root(cidx.root());
        const diff merged_diff = local_diff + downloaded;
        merged_diff.check_conflicts(); // throw an exception if has one
        idx += merged_diff;
    } else {
        idx += downloaded;
    }
    // cleanup
    bfs::remove_all(download_dir);
}

const char* download_processor::download_dir_name() {
    return k_download_dir_name;
}

}
