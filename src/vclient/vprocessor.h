#ifndef VPROCESSOR_H
#define VPROCESSOR_H

#include "global.h"

namespace verso {

class data_source;
class local_index;
class local_changes_index;

class vprocessor {
    std::string _remote_url;
    bfs::path _root;
    std::unique_ptr<local_index> _local_index;
    std::unique_ptr<local_changes_index> _local_changes_index;
    std::unique_ptr<data_source> _data_source;
public:
    explicit vprocessor(const std::string& remote, const bfs::path& root);
    virtual ~vprocessor();

    virtual void run();

protected:
    data_source& get_data_source();
    local_index& get_local_index();
    local_changes_index& get_local_changes_index();
    const bfs::path& root() const { return _root; }

private:

};

}

#endif // VPROCESSOR_H
