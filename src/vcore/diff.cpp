#include "diff.h"
#include "index.h"
#include "diff.pb.h"
#include "utils.h"
#include <boost/filesystem/fstream.hpp>

namespace verso {

namespace {
const char* const k_out_dir_name = "diff";
const char* const k_out_files_dir_name = "files";
const char* const k_diff_file_name = ".verso_diff.idx";
const char* const k_diff_pack_file_name = "diff.tgz";
}

diff::diff(const bfs::path& path) {
    bfs::path diff_path(path);
    if (path.filename() == k_diff_pack_file_name) {
        verso::uncompress_pack(path, path.parent_path());
        diff_path = path.parent_path() / k_out_dir_name / k_diff_file_name;
        set_root(path.parent_path() / k_out_dir_name / k_out_files_dir_name);
    }
    open(diff_path);
}

diff::diff(const index& newi, const index& oldi) {
    _from_sha = oldi.sha();
    _to_sha = newi.sha();
    index::kv_action_t newi_callback = [&](const std::string& sha, const std::string& path) {
        const std::string& old_sha = oldi.get_sha(path);
        operation op;
        if (old_sha.empty()) {
            op = operation::add(sha, path);
        } else {
            op = operation::change(sha, path);
        }
        insert(op);
    };
    newi.walk_items(newi_callback);

    index::kv_action_t oldi_callback = [&](const std::string& sha, const std::string& path) {
        const std::string& new_sha = newi.get_sha(path);
        operation op;
        if (new_sha.empty()) {
            op = operation::remove(sha);
        } else {
            return;
        }
        insert(op);
    };
    oldi.walk_items(oldi_callback);
}

diff::diff(const diff& lhv, const diff& rhv) {
    set_root(lhv._root);
    _operations = lhv._operations;
    _conflicts = lhv._conflicts;
    _from_sha = lhv._from_sha;
    _to_sha = rhv._to_sha;
    bool with_files = bfs::exists(_root);
    for (const auto& rop_item: rhv._operations) {
        const operation& rop = rop_item.second;
        const bool conflict = insert(rop);
        if (with_files && rop._type != operation::type::REMOVE) {
            const bfs::path in = rhv._root / rop._path;
            bfs::path out = _root / rop._path;
            if (conflict) {
                out += _to_sha;
            }
            verso::copy_file(in, out);
        }
    }
}

void diff::walk_items(diff::op_action_t callback) const {
    for (const operations_t::value_type& kv: _operations) {
        callback(kv.second);
    }
}

void diff::set_root(const boost::filesystem::path& dir) {
    _root = dir;
}

const boost::filesystem::path &diff::root() const
{
    return _root;
}

bool diff::empty() const {
    return _operations.empty();
}

boost::filesystem::path diff::pack(const boost::filesystem::path& dir) {
    const bfs::path out_dir = dir / k_out_dir_name;
    bfs::create_directory(out_dir);
    save(out_dir / k_diff_file_name);
    const bfs::path out_files_dir(out_dir / k_out_files_dir_name);
    bfs::create_directory(out_files_dir);
    for (const auto& op_item: _operations) { // could be paralelled
        const operation& op = op_item.second;
        if (op._type == operation::type::REMOVE)
            continue;
        const bfs::path& fpath_in = _root / op._path;
        const bfs::path& fpath_out = out_files_dir / op._path;
        const bfs::path fdir_out = bfs::path(fpath_out).remove_filename();
        bfs::create_directories(fdir_out);

        /**
         * Unfortunately I can not use boost here due to: https://svn.boost.org/trac/boost/ticket/10038 .
         * Fixed version is not released under Ubuntu yet.
         * I do not have time to rebuild boost with c++11 ownself - So, I'm using workaround here.
         */
        // bfs::copy_file(fpath_in, fpath_out, bfs::copy_option::fail_if_exists);
        verso::copy_file(fpath_in, fpath_out);
    }
    const bfs::path out_pack_file_path(dir / k_diff_pack_file_name);
    verso::compress_dir(out_dir, out_pack_file_path);
    return out_pack_file_path;
}

void diff::open(const boost::filesystem::path& path) {
    pb::diff pbdiff;
    bfs::ifstream pbifs(path, bfs::ifstream::binary);
    if (!pbifs) {
        std::stringstream ss;
        ss << "could not open file " << path;
        throw std::logic_error(ss.str());
    } else if (!pbdiff.ParseFromIstream(&pbifs)) {
        std::stringstream ss;
        ss << "failed to parse file: " << path;
        throw std::logic_error(ss.str());
    }
    for(int i = 0, sz = pbdiff.diff_items_size(); i < sz; ++i) {
        const pb::diff_item& pbitem = pbdiff.diff_items(i);
        switch (pbitem.type()) {
        case 0: {
            const operation op = operation::add(pbitem.sha(), pbitem.path());
            insert(op); }
            break;
        case 1: {
            const operation op = operation::change(pbitem.sha(), pbitem.path());
            insert(op); }
            break;
        case 2: {
            const operation op = operation::remove(pbitem.path());
            insert(op); }
        default:
            throw std::logic_error("Unknown operation type specified.");
            break;
        };
    }
    _from_sha = pbdiff.from_sha();
    _to_sha = pbdiff.to_sha();
    _path = path;
}

void diff::save(const boost::filesystem::path& path) {
    pb::diff pbdiff;
    pbdiff.set_from_sha(_from_sha);
    pbdiff.set_to_sha(_to_sha);
    for (const auto& op_item: _operations) {
        const operation& op = op_item.second;
        pb::diff_item* pbditem = pbdiff.add_diff_items();
        pbditem->set_type(static_cast<std::int8_t> (op._type));
        pbditem->set_path(op._path);
        if (op._type != operation::type::REMOVE)
            pbditem->set_sha(op._sha);
    }
    bfs::ofstream pbofs(path, bfs::ofstream::trunc | bfs::ofstream::binary);
    if (!pbdiff.SerializeToOstream(&pbofs)) {
        std::stringstream ss;
        ss << "Failed to serialize diff to: " << path;
        throw std::logic_error(ss.str());
    }
}

void diff::check_conflicts() const {
    if (!_conflicts.empty()) {
        cout << "Conflicts: " << endl;
        for(const std::string& path: _conflicts) {
            auto fit = _operations.find(path);
            while (fit != _operations.end() && fit->first == path) {
                cout << fit->second;
                ++fit;
                if (fit != _operations.end())
                    cout << " - ";
            }
            cout << endl;
        }
        throw std::logic_error("Conflicts above should be resolved manually.");
    }
}

const char* diff::diff_pack_file_name() {
    return k_diff_pack_file_name;
}

bool diff::insert(const diff::operation& op) {
    bool is_conflicting = false;
    auto fit = _operations.find(op._path);
    if (fit != _operations.end()) {
        _conflicts.insert(op._path);
        is_conflicting = true;
    }
    _operations.insert(operations_t::value_type(op._path, op));
    return is_conflicting;
}

diff operator - (const index& newi, const index& oldi) {
    return diff(newi, oldi);
}

diff::operation diff::operation::add(const std::string& sha, const std::string& path) {
    diff::operation op = {type::ADD, sha, path};
    return op;
}

diff::operation diff::operation::remove(const std::string& sha) {
    diff::operation op = {type::REMOVE, sha, std::string()};
    return op;
}

diff::operation diff::operation::change(const std::string& sha, const std::string& path) {
    diff::operation op = {type::CHANGE, sha, path};
    return op;
}

std::ostream& operator << ( std::ostream& stream, const diff::operation::type& rhs ) {
    switch (rhs) {
    case diff::operation::type::ADD:
        stream << "+";
        break;
    case diff::operation::type::CHANGE:
        stream << "*";
        break;
    case diff::operation::type::REMOVE:
        stream << "-";
        break;
    default:
        throw std::logic_error("Unknow type specified.");
        break;
    };
    return stream;
}

std::ostream& operator << ( std::ostream& stream, const diff::operation& rhs ) {
    stream << "<" << rhs._type << "> ";
    stream << rhs._path;
    if (rhs._type != diff::operation::type::REMOVE)
        stream << " [" << rhs._sha << "]";
    return stream;
}

diff operator + (const diff& lhv, const diff& rhv) {
    return diff(lhv, rhv);
}




}
