#include "utils.h"

#include <iomanip>
#include <openssl/sha.h>
#include <boost/filesystem/fstream.hpp>
#include <cstdlib>

namespace verso {

scoped_set_current_path::scoped_set_current_path(const boost::filesystem::path& new_path)
{
    _old_path = bfs::current_path();
    bfs::current_path(new_path);
}

scoped_set_current_path::~scoped_set_current_path()
{
    bfs::current_path(_old_path);
}

std::string sha256(const boost::filesystem::path& path)
{
    bfs::ifstream ifs(path, bfs::ifstream::binary);
    if (!ifs) {
        std::stringstream ss;
        ss << "could not open file " << path;
        throw std::logic_error(ss.str());
    }

    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);

    const size_t buff_size = 32768;
    std::vector<char> buffer(buff_size);
    size_t bytes_read = 0;

    while((bytes_read = ifs.readsome(buffer.data(), buff_size)) > 0) {
        SHA256_Update(&sha256, static_cast<const void*> (buffer.data()), bytes_read);
    }
    SHA256_Final(hash, &sha256);

    std::stringstream ss;
    for(int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        ss << std::hex << std::setw(2) << std::setfill('0') << (int)hash[i];
    }
    return ss.str();
}

std::string read_sha256(const boost::filesystem::path& path) {
    std::string result;
    if (bfs::exists(path)) {
        bfs::ifstream ifs(path, bfs::ifstream::binary);
        if (ifs) {
            result = std::string((
                        std::istreambuf_iterator<char>(ifs)),
                        (std::istreambuf_iterator<char>()));
        }
    }
    return result;
}

void copy_file(const bfs::path& from, const bfs::path& to) {
    bfs::ifstream  src(from, bfs::ifstream::binary);
    bfs::ofstream  dst(to, bfs::ofstream::binary | bfs::ofstream::trunc);
    dst << src.rdbuf();
}

void compress_dir(const bfs::path& in, const bfs::path& out) {
    const bfs::path in_parent = in.parent_path();
    std::stringstream cmd;
    cmd << "tar -C " << in_parent.native();
    cmd << " -zcf " << out.native() << " " << in.filename().native();
    const std::string cmds = cmd.str();
    ::system(cmds.c_str());
}

void uncompress_pack(const boost::filesystem::path& in, const boost::filesystem::path& out) {
    std::stringstream cmd;
    cmd << "tar -C " << out.native();
    cmd << " -zxf " << in.native();
    const std::string cmds = cmd.str();
    ::system(cmds.c_str());
}

}
