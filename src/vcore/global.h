#ifndef __VC_GLOBAL_H__
#define __VC_GLOBAL_H__

#pragma once

#include <string>
#include <fstream>
#include <vector>
#include <memory>
#include <sstream>
#include <exception>
#include <iostream>
#include <functional>
#include <cstdint>
#include <unordered_set>
#include <unordered_map>

using std::cout;
using std::endl;

#define BOOST_ALL_NO_LIB
#include <boost/noncopyable.hpp>
#include <boost/program_options.hpp>
namespace bpo = boost::program_options;
#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>
namespace bfs = boost::filesystem;
#include <boost/regex.hpp>

#if defined(_MSC_VER)
# define V_FUNC_INFO __FUNCSIG__
#else
# define V_FUNC_INFO __PRETTY_FUNCTION__
#endif

#define V_UNUSED(x) (void)x;


#endif // __VC_GLOBAL_H__
