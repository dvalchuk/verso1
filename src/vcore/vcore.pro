#-------------------------------------------------
#
# Project created by QtCreator 2015-03-04T21:47:49
#
#-------------------------------------------------

QT       -= core gui

TARGET = vcore
TEMPLATE = lib
CONFIG += staticlib

QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXX = ccache g++

SOURCES += \
    diff.cpp \
    index.cpp \
    utils.cpp

HEADERS += \
    diff.h \
    global.h \
    index.h \
    utils.h


#
# Google ProtoBuf Compiler sources
#
PBCC_SOURCES = \
        ../../src/vcore/proto/index.proto \
        ../../src/vcore/proto/diff.proto

pbcc.input = PBCC_SOURCES
pbcc.output = ../../src/vcore/${QMAKE_FILE_BASE}.pb.cc
pbcc.commands = protoc --proto_path=../../src/vcore/proto --cpp_out=../../src/vcore ${QMAKE_FILE_IN}
pbcc.variable_out = SOURCES
pbcc.CONFIG += target_predeps no_link


PBH_SOURCES = \
        ../../src/vcore/proto/index.proto \
        ../../src/vcore/proto/diff.proto

pbh.input = PBH_SOURCES
pbh.output = ../../src/vcore/proto/${QMAKE_FILE_BASE}.pb.h
pbh.commands = @echo Depending on ${QMAKE_FILE_BASE}.pb.h..
pbh.variable_out = HEADERS
pbh.CONFIG += target_predeps no_link

QMAKE_EXTRA_COMPILERS += pbcc pbh

DISTFILES += \
    proto/index.proto \
    proto/diff.proto
