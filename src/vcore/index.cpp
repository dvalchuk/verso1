#include "index.h"
#include "index.pb.h"
#include "utils.h"
#include <boost/filesystem/fstream.hpp>

namespace verso {

namespace {
const char* const k_index_dir_name = ".verso";
const char* const k_hash_extension = ".sha";
static const std::string k_empty;
}

index::index(const boost::filesystem::path& ipath)
    : _path(ipath) {
}

const char* index::index_dir_name() {
    return k_index_dir_name;
}

bfs::path index::sha_path(const bfs::path& in) {
    bfs::path temp(in);
    temp.replace_extension(k_hash_extension);
    return temp;
}

void index::walk_items(index::kv_action_t callback) const {
    for(const storage_t::value_type it : _storage) {
        callback(it.second, it.first);
    }
}

const std::string& index::get_sha(const std::string& path) const {
    storage_t::const_iterator fit = _storage.find(path);
    if (fit != _storage.end())
        return fit->second;
    return k_empty;
}

void index::update_with_index(const index& newi) {
    _storage = newi._storage;
    save();
}

void index::apply(const diff::operation &op, const boost::filesystem::path &in_root) {
    V_UNUSED(in_root)
    if (op._type == diff::operation::type::ADD || op._type == diff::operation::type::CHANGE) {
        insert(op._sha, op._path);
    } else if (op._type == diff::operation::type::REMOVE) {
        auto fit = _storage.find(op._path);
        if (fit != _storage.end()) {
            _storage.erase(fit);
        }
    }
}

void index::apply(const diff& diff) {
    if (!bfs::exists(diff.root())) {
        std::stringstream ss;
        ss << "diff's root [" << _path << "] does not exists.";
        throw std::logic_error(ss.str());
    }
    diff::op_action_t callback = [&] (const diff::operation& op) {
        apply(op, diff.root());
    };
    diff.walk_items(callback);
    save();
}

index& index::operator += (const diff& diff) {
    apply(diff);
    return *this;
}

void index::save() {
}

void index::open() {
    pb::index pbindex;
    bfs::ifstream pbifs(_path, bfs::ifstream::binary);
    if (!pbifs) {
        std::stringstream ss;
        ss << "could not open file " << _path;
        throw std::logic_error(ss.str());
    } else if (!pbindex.ParseFromIstream(&pbifs)) {
        std::stringstream ss;
        ss << "failed to parse file: " << _path;
        throw std::logic_error(ss.str());
    }
    for(int i = 0, sz = pbindex.index_items_size(); i < sz; ++i) {
        const pb::index_item& pbitem = pbindex.index_items(i);
        insert(pbitem.sha(), pbitem.path());
    }
    _sha = read_sha256(sha_path());
}

void index::insert(const std::string& sha, const std::string& path)
{
    _storage.insert(storage_t::value_type(path, sha));
}

boost::filesystem::path index::sha_path() const {
    return index::sha_path(_path);
}

}
