#ifndef UTILS_H
#define UTILS_H

#include "global.h"

namespace verso {

class scoped_set_current_path {
    bfs::path _old_path;
public:
    explicit scoped_set_current_path(const bfs::path& new_path);
    ~scoped_set_current_path();
};

std::string sha256(const bfs::path& path);
std::string read_sha256(const bfs::path& path);
void copy_file(const bfs::path& from, const bfs::path& to);
void compress_dir(const bfs::path& in, const bfs::path& out);
void uncompress_pack(const bfs::path& in, const bfs::path& out);
}

#endif // UTILS_H
