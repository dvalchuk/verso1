#if !defined(DIFF_H_)
#define DIFF_H_

#include "global.h"

namespace verso {

class index;

class diff {
public:

    struct operation {
        enum class type : std::int8_t {
            ADD = 0,
            CHANGE,
            REMOVE
        };

        static operation add(const std::string& sha, const std::string& path);
        static operation remove(const std::string& sha);
        static operation change(const std::string& sha, const std::string& path);

        type        _type;
        std::string _sha;
        std::string _path;
    };

private:
    bfs::path _path;
    bfs::path _root;
    typedef std::unordered_multimap<std::string, operation> operations_t;
    operations_t _operations;
    typedef std::unordered_set<std::string> conflicts_t;
    conflicts_t _conflicts;
    std::string _from_sha;
    std::string _to_sha;

public:
    explicit diff(const bfs::path& path);
    explicit diff(const index& newi, const index& oldi);
    explicit diff(const diff& lhv, const diff& rhv);

    typedef std::function< void(const operation& op) > op_action_t;
    void walk_items(op_action_t callback) const;

    void set_root(const bfs::path& dir);
    const bfs::path& root() const;
    bool empty() const;
    operator bool() const { return !empty(); }
    bool operator !() const { return empty(); }

    bfs::path pack(const bfs::path& dir);

    void open(const bfs::path& path);
    void save(const bfs::path& path);

    void check_conflicts() const;

    static const char* diff_pack_file_name();

private:
    bool insert(const operation& op);

};

std::ostream& operator << ( std::ostream& stream, const diff::operation::type& rhs );
std::ostream& operator << ( std::ostream& stream, const diff::operation& rhs );

diff operator - (const index& newi, const index& oldi);
diff operator + (const diff& lhv, const diff& rhv);


}


#endif // DIFF_H_
