#ifndef INDEX_H
#define INDEX_H

#include "global.h"
#include "diff.h"

namespace verso {


class index {
    typedef std::unordered_map<std::string, std::string> storage_t;
    bfs::path _path;
    std::string _sha;
    // it is better to use sqlite to decrease memory usage (depends on index size)
    storage_t _storage;

public:
    explicit index(const boost::filesystem::path& ipath);
    virtual ~index() {}

    const std::string sha() const { return _sha; }

    typedef std::function< void(const std::string& sha, const std::string& path) > kv_action_t;
    void walk_items(kv_action_t callback) const;

    const std::string& get_sha(const std::string& path) const;
    const std::string& get_path(const std::string& sha) const;

    void update_with_index(const index& newi);

    void apply(const diff& diff);
    index& operator += (const diff& diff);

    static const char* index_dir_name();
    static bfs::path sha_path(const bfs::path& in);

protected:
    virtual void apply(const diff::operation& op, const bfs::path& in_root);
    virtual void save();
    void open();
    void insert(const std::string& sha, const std::string& path);
    const bfs::path& path() const { return _path; }
    void set_sha(const std::string& sha) { _sha = sha; }
    bfs::path sha_path() const;
};

}

#endif // INDEX_H
